module github.com/DidierStockmans/shippy-service-consignment

go 1.12

require (
	github.com/DidierStockmans/shippy-service-vessel v0.0.0-20190715185504-64aee02b2a1d // indirect
	github.com/golang/protobuf v1.3.2
	github.com/micro/go-micro v1.7.0
	github.com/micro/protobuf v0.0.0-20180321161605-ebd3be6d4fdb // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7
	golang.org/x/sys v0.0.0-20190712062909-fae7ac547cb7 // indirect
	google.golang.org/genproto v0.0.0-20190708153700-3bdd9d9f5532 // indirect
	google.golang.org/grpc v1.22.0
)
